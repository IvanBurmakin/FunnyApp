package com.funnyapp.funnyapp;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.funnyapp.funnyapp.model.AnekdotModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class BlankFragment extends Fragment implements View.OnClickListener {

    @BindView(R.id.editText)
    EditText editText;
    @BindView(R.id.btn_load)
    Button btnLoad;
    @BindView(R.id.btn_clear)
    Button btnClean;
    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;


    private View view;
    private RestAPI restAPI;
    private ArrayList<AnekdotModel> list;
    private Unbinder unbinder;
    private MyAdapter myAdapter;

    private static final String LIST_STATE_KEY = "List";
    private static final String EMPTY_STRING = "";
    private static final String FONT_CUPHEADMEMPHIS = "cupheadmemphis.ttf";



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        view = inflater.inflate(R.layout.fragment_blank, container, false);

        init();

        if (savedInstanceState != null) {
            list = (ArrayList<AnekdotModel>) savedInstanceState.getSerializable(LIST_STATE_KEY);
            if (list != null) {
                myAdapter.fillList(list);
            }
        }

        return view;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        if (list != null) {
            outState.putSerializable(LIST_STATE_KEY, list);
        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    private void init() {
        restAPI = MyRetrofit.makeRetrofit();
        initUi();
        initRecView();

    }

    private void initUi() {
        unbinder = ButterKnife.bind(this, view);
        btnLoad.setOnClickListener(this);
        btnClean.setOnClickListener(this);
    }

    private void initRecView() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        myAdapter = new MyAdapter();
        mRecyclerView.setAdapter(myAdapter);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_load:
                if (!editText.getText().toString().isEmpty()) {
                    short number = Short.parseShort(editText.getText().toString());
                    editText.setText(EMPTY_STRING);
                    downloadData(number);
                } else {
                    showSnackbar(getString(R.string.empty_edit_text));
                }
                break;
            case R.id.btn_clear:
                myAdapter.clearList();
                break;
        }
    }

    private void downloadData(short number) {
        Call<List<AnekdotModel>> call = restAPI.loadUmor(number);
        call.enqueue(new Callback<List<AnekdotModel>>() {
            @Override
            public void onResponse(Call<List<AnekdotModel>> call, Response<List<AnekdotModel>> response) {

                if (response.isSuccessful()) {
                    list = new ArrayList<>();
                    list.addAll(response.body());
                    clearString(list);
                    myAdapter.setTypeface(makeTypeface(FONT_CUPHEADMEMPHIS));
                    myAdapter.fillList(list);
                } else {
                    showSnackbar(getString(R.string.unsuccessful_response));
                    Log.d("unSuccessful Response", response.errorBody().toString());
                }
            }

            @Override
            public void onFailure(Call<List<AnekdotModel>> call, Throwable t) {
                Log.d("onFailure Error", t.getMessage().toString());
            }
        });
    }


    private void showSnackbar(String value) {
        Snackbar.make(view, value, Snackbar.LENGTH_LONG)
                .show();
    }

    private void clearString(List<AnekdotModel> list) {
        for (AnekdotModel model : list) {
            String elementPureHtml = model.getElementPureHtml();

            elementPureHtml = elementPureHtml.replaceAll("<br />", EMPTY_STRING);
            elementPureHtml = elementPureHtml.replaceAll("&quot;", EMPTY_STRING);
            elementPureHtml = elementPureHtml.replaceAll("&laquo;", EMPTY_STRING);
            elementPureHtml = elementPureHtml.replace("&raquo;", EMPTY_STRING);

            model.setElementPureHtml(elementPureHtml);
        }
    }

    private Typeface makeTypeface(String fontName) {
        Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/" + fontName);
        typeface.isBold();
        return typeface;
    }


}
