package com.funnyapp.funnyapp;

import android.app.Application;

import cat.ereza.customactivityoncrash.config.CaocConfig;

public class CustomCrash extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        CaocConfig.Builder.create()
                .backgroundMode(CaocConfig.BACKGROUND_MODE_SILENT)
                .enabled(true)
                .showErrorDetails(false)
                .showRestartButton(true)
                .logErrorOnRestart(true)
                .trackActivities(false)
                .minTimeBetweenCrashesMs(2000)
                .errorDrawable(R.drawable.crash)
                .restartActivity(MainActivity.class)
                .apply();
    }
}
