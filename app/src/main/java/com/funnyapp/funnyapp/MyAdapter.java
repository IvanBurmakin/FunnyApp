package com.funnyapp.funnyapp;

import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.funnyapp.funnyapp.model.AnekdotModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {


    private ArrayList<AnekdotModel> list = new ArrayList<>();
    private Typeface typeface;


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.anekdot_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        AnekdotModel model = list.get(position);
        holder.bind(model);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void clearList() {
        list.clear();
        notifyDataSetChanged();
    }

    public void fillList(ArrayList<AnekdotModel> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public void setTypeface( Typeface typeface){
        this.typeface = typeface;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.site)
        TextView site;
        @BindView(R.id.desc)
        TextView desc;
        @BindView(R.id.elementPureHtml)
        TextView elementPureHtml;

        AnekdotModel model;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(AnekdotModel model) {
            this.model = model;
            site.setText(model.getSite());
            desc.setText(model.getDesc());
            elementPureHtml.setTypeface(typeface);
            elementPureHtml.setText(model.getElementPureHtml());
        }
    }

}

