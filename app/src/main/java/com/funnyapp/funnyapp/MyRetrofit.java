package com.funnyapp.funnyapp;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MyRetrofit {

    private static final String BASE_URL_UMORILI = "http://umorili.herokuapp.com/api/";


    public static RestAPI makeRetrofit() {


        Gson gson = new GsonBuilder()
                .setLenient()
                .create();


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL_UMORILI)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        RestAPI restAPI = retrofit.create(RestAPI.class);
        return restAPI;
    }

}
