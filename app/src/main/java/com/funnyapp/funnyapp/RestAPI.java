package com.funnyapp.funnyapp;


import com.funnyapp.funnyapp.model.AnekdotModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RestAPI {


    @GET("get?site=bash.im&name=bash&")
    Call<List<AnekdotModel>> loadUmor(@Query("num") short number);


}
